function clearForm() {
    $("#name").val("");
    $("#email").val("");
    $("#message").val("");
    $("#policy").prop("checked", false);
    $("#sendButton").addClass("disabled_button");
    localStorage.clear();
}

function saveLocalStorage() {
    localStorage.setItem("name", $("#name").val());
    localStorage.setItem("email", $("#email").val());
    localStorage.setItem("message", $("#message").val());
    localStorage.setItem("policy", $("#policy").is(":checked"));
}

function loadLocalStorage() {
    if (localStorage.getItem("name") !== null) {
        $("#name").val(localStorage.getItem("name"));
    }
    if (localStorage.getItem("email") !== null) {
        $("#email").val(localStorage.getItem("email"));
    }
    if (localStorage.getItem("message") !== null) {
        $("#message").val(localStorage.getItem("message"));
    }
    if (localStorage.getItem("policy") !== null) {
        if (localStorage.getItem("policy") === "true") {
            $("#policy").prop("checked", true);
        } else {
            $("#policy").prop("checked", false);
        }
        if ($("#policy").is(":checked")) {

            $("#sendButton").removeClass("disabled_button");
        } else {

            $("#sendButton").addClass("disabled_button");
        }
    }
}

$(document).ready(function() {
    loadLocalStorage();
    $("#openButton").click(function() {
        $("#overlayPOP").css("display", "flex");
        history.pushState(true, "", "./form");
    });
    $("#closeButton").click(function() {
        $("#overlayPOP").css("display", "none");
        history.pushState(false, "", ".");
    });
    $("#popup_form").submit(function(e) {
        e.preventDefault();
        if ($("#name").val() === "") {
            alert("Введите ваши контактные данные");
        } else {
            $("#overlayPOP").css("display", "none");
            let data = $(this).serialize();
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "https://formcarry.com/s/VHCXWSUxl8",
                data: data,
                success: function(response) {
                    if (response.status === "success") {
                        alert("Спасибо, мы скоро вам перезвоним");
                        clearForm();
                    } else {
                        alert("Произошла ошибка: " + response.message);
                    }
                }
            });
            history.pushState(false, "", ".");
        }
    });
    $("#policy").change(function() {
        if (this.checked) {
            $("#sendButton").removeClass("disabled_button");
        } else {
            $("#sendButton").addClass("disabled_button");
        }
    });
    $("#popup_form").change(saveLocalStorage);

    window.onpopstate = function(event) {
        if (event.state) {
            $("#overlayPOP").css("display", "flex");
        } else {
            $("#overlayPOP").css("display", "none");
        }
    };
});